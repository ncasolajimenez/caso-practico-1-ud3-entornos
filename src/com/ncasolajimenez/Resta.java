package com.ncasolajimenez;

public class Resta {
	private int num1;
	private int num2;
	
	public Resta(int num1, int num2) {

		this.num1 = num1;
		this.num2 = num2;
	}
	
	public int restar() {
		int resultado = num1 - num2;
		return resultado;
	}
	
}
