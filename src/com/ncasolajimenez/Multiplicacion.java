package com.ncasolajimenez;

public class Multiplicacion {
	private int num1;
	private int num2;
	
	public Multiplicacion(int num1, int num2) {

		this.num1 = num1;
		this.num2 = num2;
	}
	
	public int multiplicar() {
		int resultado = num1 * num2;
		return resultado;
	}
	
}
